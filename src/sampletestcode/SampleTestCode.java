/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sampletestcode;

/**
 *
 * @author Fuck Boy Nation
 */
public class SampleTestCode {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Object obj = new String("Hello");
        
        RotatedBox box1 = new RotatedBox();
        RotatedBox box2 = new RotatedBox();
        box1.x = 1;
        box2.x = 2;
        box1 = box2;
        box1.x = 3;
        box2.x = 4;
        System.out.println(box1.x);
        System.out.println(box2.x);
        
        AABB box = new AABB();
        box.x = 5;
        int a = box.x;
        int b = a;
        a++;
        reset(box, box, a, b);
        System.out.println(a);
        System.out.println(b);
        System.out.println(box.x);
        
        Num n = new Num();
        DumbNum dN = new DumbNum();
        n = new DumbNum();
        //dN = new Num(); THIS IS A SYNTAX  ERROR FOR QUESTION 35
        n = (Num) new DumbNum();
        //dN = (DumbNum) new Num(); THIS IS THE COMPILER ERROR CANNOT INSTANTIATE A CHILD CLASS AS A PARENT CLASS AND THEN TRY TO CAST IT AS A CHILD(?)
        n = (DumbNum) new DumbNum();
        
        Num num = new Num();    
        num.update();
        System.out.println(num.n);
        Num num2 = new DumbNum();
        num2.update();
        System.out.println(num2.n);
        Num num3 = new DumberNum();
        num3.update();
        System.out.println(num3.n);
        Num num4 = new DumbestNum();
        num4.update();
        System.out.println(num4.n);

    }
    
    public static void reset (AABB box1, AABB box2, int a, int b) {
        box1 = new AABB();
        box1.x = 10;
        box2.x = 10;
        a = 20;
        b = 20;
    }
    
}
